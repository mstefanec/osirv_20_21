# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 20:43:15 2020

@author: Mia
"""

import cv2


img_boats = cv2.imread('../../slike/boats.bmp',0)
img_baboon = cv2.imread('../../slike/baboon.bmp',0)
img_airplane = cv2.imread('../../slike/airplane.bmp',0)

img_list = [img_boats,img_baboon,img_airplane]
img_names =['boats','baboon','airplane']

for i in range(len(img_list)):
    ret, thresh1 = cv2.threshold(img_list[i], 127, 255, cv2.THRESH_BINARY)
    cv2.imwrite(img_names[i] + '_thresh_binary' + '.jpg', thresh1)
    ret, thresh2 = cv2.threshold(img_list[i], 127, 255, cv2.THRESH_BINARY_INV)
    cv2.imwrite(img_names[i] + '_thresh_binary_inv' + '.jpg', thresh2)
    ret, thresh3 = cv2.threshold(img_list[i], 127, 255, cv2.THRESH_TRUNC)
    cv2.imwrite(img_names[i] + '_thresh_trunc' + '.jpg', thresh3)
    ret, thresh4 = cv2.threshold(img_list[i], 127, 255, cv2.THRESH_TOZERO)
    cv2.imwrite(img_names[i] + '_thresh_tozero' + '.jpg', thresh4)
    ret, thresh5 = cv2.threshold(img_list[i], 127, 255, cv2.THRESH_TOZERO_INV)
    cv2.imwrite(img_names[i] + '_thresh_tozero_inv' + '.jpg', thresh5)
    th6 = cv2.adaptiveThreshold(img_list[i], 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(img_names[i] + '_thresh_adaptive_mean' + '.jpg', th6)
    th7 = cv2.adaptiveThreshold(img_list[i], 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(img_names[i] + '_thresh_adaptive_gaussian' + '.jpg', th7)
    ret, th8 = cv2.threshold(img_list[i], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    cv2.imwrite(img_names[i] + '_thresh_otsu' + '.jpg', th8)
