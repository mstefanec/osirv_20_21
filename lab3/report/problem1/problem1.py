# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 19:19:07 2020

@author: Mia
"""

import numpy as np

import matplotlib.pyplot as plt

import cv2

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
  
def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()


img = cv2.imread('../../slike/goldhill.bmp')

gauss_sigmas = np.array([1, 5, 10, 20, 40, 60])

for sigma in gauss_sigmas:
    img = gaussian_noise(img, 0 , sigma)
    showhist(img)
    cv2.imwrite('gaussian_noise'+str(sigma)+'.bmp',img)

uniform_range=[(-20,20),(-40,40),(-60,60)]

for r in uniform_range:
    img = uniform_noise(img, r[0], r[1])
    hist.title('')
    showhist(img)
    cv2.imwrite('uniform_noise'+str(r)+'.bmp',img)
    
percents=[5, 10, 15, 20]

for percent in percents:
    img = salt_n_pepper_noise(img,percent)
    showhist(img)
    cv2.imwrite('salt_n_papper_noise'+str(percent)+'.bmp', img)