# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 18:23:18 2020

@author: Mia
"""

import cv2

img = cv2.imread('../../slike/one_coin.jpg',cv2.IMREAD_GRAYSCALE)
blurred = cv2.GaussianBlur(img, (5,5), 0)

#lower=0,upper=255
coin_edges = cv2.Canny(blurred,0,255)
cv2.imwrite('coin_canny_0_255.jpg',coin_edges)

#lower=128,upper=128
coin_edges = cv2.Canny(blurred,128,128)
cv2.imwrite('coin_canny_128_128.jpg',coin_edges)

#lower=128,upper=128
coin_edges = cv2.Canny(blurred,255,255)
cv2.imwrite('coin_canny_255_255.jpg',coin_edges)

"""najbolji rezultat daje kombinacija (0,255) jer su rubovi najvidljiviji,iako je
kombinacija (128,128) uhvatila neke rubove koje prva nije""" 