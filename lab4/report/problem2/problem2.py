# -*- coding: utf-8 -*-
"""
Created on Thu Jan  7 12:28:07 2021

@author: Mia
"""

import numpy as np
import cv2

def auto_canny(image, sigma=0.33):
        v = np.median(image)
        lower = int(max(255, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print ("Median lower: %r." % lower)
        print ("Median upper: %r." % upper)
        # return the edged image
        return edged
    
images = [cv2.imread("../../slike/airplane.bmp",0), cv2.imread("../../slike/barbara.bmp",0), cv2.imread("../../slike/boats.bmp",0), cv2.imread("../../slike/pepper.bmp",0)]
img_names = ['airplane', 'barbara', 'boats', 'pepper']

for i in range(4):
    print(img_names[i])
    auto = auto_canny(images[i])
    cv2.imwrite(img_names[i] + '_255-255_cannyedge.jpg', auto)
