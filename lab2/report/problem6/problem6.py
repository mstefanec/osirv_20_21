# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 22:10:15 2020

@author: Mia
"""

import cv2

import numpy as np

boatsC = cv2.imread('../../slike/BoatsColor.bmp', cv2.IMREAD_GRAYSCALE)

for q in range(1,9):
    boats_img = np.array(boatsC)
    power = 2 ** int(q)
    noise = np.random.uniform(-0.5,0.5,boats_img.shape)
    boats_img = boats_img.astype(np.float32)
    boats_img = (np.floor(boats_img/power + 0.5 + noise) * power)
    boats_img[ boats_img > 255 ] = 255
    boats_img[ boats_img < 0 ] = 0
    boats_img = boats_img.astype(np.uint8)
    cv2.imwrite(('boats_' + str(q) + '.bmp'),boats_img)
    
#sum se nadzire vec pri vrijednosti q=2,a nakon toga je sve vidljiviji
#pri q=8,lakse se moze raznanti sto je na slici nego sto je to bilo u prethodnom zadatku