# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 14:51:20 2020

@author: Mia
"""

import numpy as np

import cv2

image1 = cv2.imread('../../slike/goldhill.bmp')
image2 = cv2.imread('../../slike/BoatsColor.bmp')
image3 = cv2.imread('../../slike/airplane.bmp')

image1_resize=image1[:512,:512]
image2_resize=image2[:512,:512]
image3_resize=image3[:512,:512]
    
new_image=np.vstack((image1_resize,image2_resize,image3_resize))

cv2.imwrite('vstack_images.bmp',new_image)



vstack_with_border=cv2.copyMakeBorder(new_image,50,50,50,50,cv2.BORDER_CONSTANT)

cv2.imwrite('vstack_border.bmp',vstack_with_border)

cv2.imshow('vstack',new_image)
cv2.imshow('vstackBorder',vstack_with_border)
cv2.waitKey(0)
cv2.destroyAllWindows()
