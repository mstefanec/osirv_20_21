# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 16:38:48 2020

@author: Mia
"""

import cv2

airplane = cv2.imread('../../slike/airplane.bmp', cv2.IMREAD_GRAYSCALE)
baboon = cv2.imread('../../slike/baboon.bmp', cv2.IMREAD_GRAYSCALE)
barbara = cv2.imread('../../slike/barbara.bmp', cv2.IMREAD_GRAYSCALE)
boats = cv2.imread('../../slike/boats.bmp', cv2.IMREAD_GRAYSCALE)
boatsC = cv2.imread('../../slike/BoatsColor.bmp', cv2.IMREAD_GRAYSCALE)
goldhill = cv2.imread('../../slike/goldhill.bmp', cv2.IMREAD_GRAYSCALE)
lenna = cv2.imread('../../slike/lenna.bmp', cv2.IMREAD_GRAYSCALE)
pepper =  cv2.imread('../../slike/pepper.bmp', cv2.IMREAD_GRAYSCALE)
#pero =  cv2.imread('../../slike/pero.bmp', cv2.IMREAD_GRAYSCALE)

airplane_invert = cv2.bitwise_not(airplane) 
baboon_invert = cv2.bitwise_not(baboon) 
barbara_invert = cv2.bitwise_not(barbara) 
boats_invert = cv2.bitwise_not(boats) 
boatsC_invert = cv2.bitwise_not(boatsC) 
goldhill_invert = cv2.bitwise_not(goldhill) 
lenna_invert = cv2.bitwise_not(lenna) 
pepper_invert = cv2.bitwise_not(pepper)
#pero_invert = cv2.bitwise_not(pero)


cv2.imwrite("airplane_invert.png",airplane_invert)
cv2.imwrite("baboon_invert.png",baboon_invert)
cv2.imwrite("barbara_invert.png",barbara_invert)
cv2.imwrite("boats_invert.png",boats_invert)
cv2.imwrite("boatsC_invert.png",boatsC_invert)
cv2.imwrite("goldhill_invert.png",goldhill_invert)
cv2.imwrite("lenna_invert.png",lenna_invert)
cv2.imwrite("pepper_invert.png",pepper_invert)
#cv2.imwrite("pero_invert.png",pero_invert)


