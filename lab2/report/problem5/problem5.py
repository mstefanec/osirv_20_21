# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 21:24:20 2020

@author: Mia
"""

import cv2

import numpy as np

boatsC = cv2.imread('../../slike/BoatsColor.bmp', cv2.IMREAD_GRAYSCALE)

for q in range(1,9):
    boats_img = np.array(boatsC)
    power = 2 ** int(q) 
    boats_img = boats_img.astype(np.float32)
    boats_img = (np.floor(boats_img/power + 0.5) * power)
    boats_img[ boats_img > 255 ] = 255
    boats_img[ boats_img < 0 ] = 0
    boats_img = boats_img.astype(np.uint8)
    cv2.imwrite(('boats_' + str(q) + '.bmp'),boats_img)
    
#promjene na slici su vec lagano vidljive pri vrijednosti q=3 na nebu,
#a za vrijednosti od 4 do 8 razlike su sve vece i vece
