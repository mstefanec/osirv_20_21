# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 18:35:12 2020

@author: Mia
"""

import cv2

import os

def threshold(img,parameter):
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if(img[i,j]<=parameter):
                img[i,j]=0
    return img

       
images = []
for file_img in os.listdir('../../slike'):
    img = cv2.imread(os.path.join('../../slike',file_img),0)
    if img is not None:
        cv2.imwrite(file_img+"_63_thresh.png", threshold(img,63))
        cv2.imwrite(file_img+"_127_thresh.png", threshold(img,127))
        cv2.imwrite(file_img+"_191_thresh.png", threshold(img,191))