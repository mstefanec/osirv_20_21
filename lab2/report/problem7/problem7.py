# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 22:26:02 2020

@author: Mia
"""

import cv2
import numpy as np

baboon_img = cv2.imread('../../slike/baboon.bmp', 0)
baboon_img = cv2.resize(baboon_img, None, fx=0.25, fy=0.25, interpolation = cv2.INTER_CUBIC)
rows, cols = baboon_img.shape

baboon_rot = []

for angle in range(0, 360, 30):
    M = cv2.getRotationMatrix2D((cols/2, rows/2), angle, 1)
    dst = cv2.warpAffine(baboon_img, M, (cols, rows))
    baboon_rot.append(dst)

rotated = np.hstack(baboon_rot)
cv2.imwrite('baboon_rotated.bmp', rotated)
