# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 16:17:34 2020

@author: Mia
"""

import numpy as np

import cv2

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 

img = cv2.imread('../../slike/airplane.bmp')

img_grayscale = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

kernel = np.array([[0,-1,0],[-1,4,-1],[0,-1,0]])

img_convolve = convolve(img_grayscale,kernel)

cv2.imwrite('airplane_convolve.bmp',img_convolve)

cv2.imshow('imgc',img_convolve)
cv2.waitKey(0)
cv2.destroyAllWindows()

cv2.imwrite('airplane_convolve.bmp',img_convolve)