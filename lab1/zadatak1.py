# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 09:59:58 2020

@author: Mia
"""

import cv2

photo = cv2.imread('slike/goldhill.bmp')

blue = photo.copy()
blue[:,:,1:] = 0
cv2.imwrite('slike/blue.jpg',blue)

green = photo.copy()
green[:,:,::2] = 0
cv2.imwrite('slike/green.jpg',green)

red = photo.copy()
red[:,:,0:2] = 0
cv2.imwrite('slike/red.jpg',red)