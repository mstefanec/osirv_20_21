# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 10:19:44 2020

@author: Mia
"""

import cv2

photo = cv2.imread('slike/goldhill.bmp')

blue = photo.copy()
blue[:,:,1:] = 0
cv2.imshow('Blue',blue)

green = photo.copy()
green[:,:,::2] = 0
cv2.imshow('Green',green)

red = photo.copy()
red[:,:,0:2] = 0
cv2.imshow('Red',red)

cv2.waitKey(0)
cv2.destroyAllWindows()
