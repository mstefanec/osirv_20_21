# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 10:26:46 2020

@author: Mia
"""

import cv2
import numpy as np

photo = cv2.imread('slike/pepper.bmp')

photo=cv2.copyMakeBorder(photo,10,10,10,10,cv2.BORDER_CONSTANT)

cv2.imwrite('slike/pepper_border.bmp',photo)