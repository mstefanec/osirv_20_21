# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 09:26:08 2020

@author: Mia
"""

streets= []


while True:
    street_name = input ("Enter the street: ")

    if street_name == "prekid":
        print("Number of streets: ",len(streets),"\nLongest name: ", max(streets, key = len))
        break
    elif (len(street_name)< 7 or len(street_name) > 15):
        print("Street name is too long! ")
    else:
        streets.append(street_name)