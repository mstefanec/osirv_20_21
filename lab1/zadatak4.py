# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 10:31:23 2020

@author: Mia
"""

import cv2

photo = cv2.imread('slike/baboon.bmp')

cv2.imshow('Vertical', photo[::2,:,:])
cv2.imshow('Horizontal', photo[:,::2,:])
cv2.imshow('Both', photo[::2,::2,:])

cv2.waitKey(0)
cv2.destroyAllWindows()
