# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 09:07:22 2020

@author: Mia
"""

import random

random_number = random.randrange(15)

print(random_number)

number = int(input("Guess the number: "))

attemps = 0

while True:
    attemps+=1
    if number != random_number:
        print("Wrong")
        number = int(input("Guess the number: "))
    else:
        print(number,"Correct number!Number of attemps: ",attemps)
        break;
        