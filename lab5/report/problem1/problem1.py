# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 17:21:00 2021

@author: Mia
"""

import cv2

cap = cv2.VideoCapture('../../road.mp4')

if (cap.isOpened()== False):
  print("Error opening video stream or file")

fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,360))

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
        frame=cv2.flip(frame, 1)
        out.write(frame)

        cv2.imshow('Frame',frame)
        k = cv2.waitKey(30) & 0xff
        if k == 27:
             break

cap.release()
out.release()
cv2.destroyAllWindows()