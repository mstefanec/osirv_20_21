# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 18:25:00 2021

@author: Mia
"""

import cv2


cap = cv2.VideoCapture(0)

# Check if camera opened successfully
if (cap.isOpened()== False):
  print("Error opening video stream or file")

while(cap.isOpened()):
  ret, frame = cap.read()
  if ret == True:

    cv2.imshow('Camera frame',frame)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
cap.release()
cv2.destroyAllWindows()
